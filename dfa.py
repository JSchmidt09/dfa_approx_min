"""Extends the DFA class of FAdo with hyperoptimization."""
from itertools import product
from FAdo.fa import DFA as FAdoDFA
from FAdo.fio import readFromFile, saveToFile

class DFA(FAdoDFA):
    """Extendet version of FAdo's DFA class"""

    def hyperoptimal(self, strict=False):
        """
        Hyperoptimization of a minimal DFA.

        Compute the hyperminimal DFA with the least number of errors.

        :returns: a hyperoptimal DFA and number of errors
        :rtype: tuple

        .. seealso::
            Maletti, Andreas und Daniel Quernheim: Optimal hyper-minimization.
            International Journal of Foundations of Computer Science,
            22(08):18771891, 2011.
        """
        if strict:
            m = self.minimal()
        else:
            m = self.dup()
        comp, center, mark = m.computeKernel()
        ker = set([m.States[s] for s in mark])
        opt_dfa = m.optMerge(ker, m.aEquiv())
        opt_dfa.trim()
        return opt_dfa, m.global_distance, m.global_distance_strings

    def _initializeMerge(self):
        self.global_distance = 0
        self.global_distance_strings = set([])

        self.error_count = {}
        for q1 in self.States:
            self.error_count[q1] = {q2:-1 for q2 in self.States}
            self.error_count[q1][q1] = 0

        self.error_strings = {}
        for q1 in self.States:
            self.error_strings[q1] = {q2:None for q2 in self.States}
            self.error_strings[q1][q1] = set()

        self.q0 = self.States[self.Initial]
        self.prefix_count = {q:0 for q in self.States}
        self.prefix_count[self.q0] = 1

        self.prefix_strings = {q:set() for q in self.States}
        self.prefix_strings[self.q0] = {""}

        self._compute_delta_inv()

    def optMerge(self, ker, aequiv):
        """
        Optimaly merge almost-equivalent states.

        :param set ker: set of all kernel states
        :param dictionary aequiv: partition of almost equivalence
        :returns: hyperoptimal dfa
        :rtype: DFA
        """
        self._initializeMerge()

        opt_dfa = self.dup()
        K_0 = self._K(self.q0, ker, aequiv)
        if K_0:
            initial_errors = {q:self.compErrorCount(self.q0, q) for q in K_0}
            new_initial = min(initial_errors, key=initial_errors.get)
            self.global_distance = self.error_count[self.q0][new_initial]
            self.global_distance_strings = self.error_strings[self.q0][new_initial]
            opt_dfa.Initial = opt_dfa.stateIndex(new_initial)
            return opt_dfa

        pre = set(self.States)-ker
        P_aequiv = self._P(pre, aequiv)
        for r, B in P_aequiv.iteritems():
            q = self.compFinality(B)
            for p in B:
                opt_dfa.mergeStates(opt_dfa.stateIndex(p), opt_dfa.stateIndex(q))
            for a in self.Sigma:
                K = self._K(self._delta(q, a), ker, aequiv)
                if K:
                    merge_errors = {k:sum([self.compPrefixCount(p)*self.compErrorCount(self._delta(p, a), k) for p in B]) for k in K}
                    self.global_distance += min(merge_errors.itervalues())
                    min_k = min(merge_errors, key=merge_errors.get)
                    self.global_distance_strings |= {prefix+a+error for p in B for (prefix, error) in product(self.compPrefixStrings(p), self.compErrorStrings(self._delta(p, a), min_k))}
                    opt_dfa.delta[opt_dfa.stateIndex(q)][a] = opt_dfa.stateIndex(min_k)
        return opt_dfa

    def compErrorCount(self, p, q):
        """
        Compute the number of errors between q and p.

        :param object p: first state
        :param object q: second state
        :returns: number of errors between q and p
        :rtype: int
        """
        if self.error_count[p][q] == -1:
            c = (self.finalP(self.stateIndex(q)) != self.finalP(self.stateIndex(p)))
            self.error_count[p][q] = c + sum([self.compErrorCount(self._delta(q, a), self._delta(p, a)) for a in self.Sigma])
            self.error_count[q][p] = self.error_count[p][q]
        return self.error_count[p][q]

    def compErrorStrings(self, p, q):
        """
        Compute the actual errors between q and p.

        :param object p: first state
        :param object q: second state
        :returns: set of errors for q and p
        :rtype: set
        """
        if self.error_strings[p][q] == None:
            if self.finalP(self.stateIndex(q)) != self.finalP(self.stateIndex(p)):
                c = {""}
            else:
                c = set([])
            errors_by_sigma = {a:self.compErrorStrings(self._delta(q, a), self._delta(p, a)) for a in self.Sigma}
            self.error_strings[p][q] = c | {a+word for a, words in errors_by_sigma.iteritems() for word in words}
            self.error_strings[q][p] = self.error_strings[p][q]
        return self.error_strings[p][q]

    def compPrefixCount(self, q):
        """
        Compute the number of prefixes to the preamble state q.

        :param int q: preamble state q
        :returns: number of paths to q
        :rtype: int
        """
        if self.prefix_count[q] == 0:
            self.prefix_count[q] = sum([self.compPrefixCount(self.States[p]) for a in self.Sigma for p in self.delta_inv[self.stateIndex(q)][a]])
        return self.prefix_count[q]

    def compPrefixStrings(self, q):
        """
        Compute the prefix strings of the preamble state q.

        :param int q: preamble state q
        :returns: set of words that lead to q
        :rtype: set
        """
        if self.prefix_strings[q] == set():
            words_by_delta = {(p, a):self.compPrefixStrings(self.States[p]) for a in self.Sigma for p in self.delta_inv[self.stateIndex(q)][a]}
            self.prefix_strings[q] = {word+a for ((p, a), words) in words_by_delta.iteritems() for word in words}
        return self.prefix_strings[q]

    def compFinality(self, B):
        """
        Compute finality of a block of preamble states.

        :param set B: set of almost equivalent preamble states
        :returns: selected state
        :rtype: object
        """
        final_states = {self.States[f] for f in self.Final}
        f_n, f = sum(self.compPrefixCount(q) for q in (B & final_states)), sum(self.compPrefixCount(q) for q in (B - final_states))
        self.global_distance += min([f_n, f])
        if f_n > f:
            selection_set = B & final_states
        else:
            selection_set = B - final_states
        self.global_distance_strings |= {prefix for p in B - selection_set for prefix in self.compPrefixStrings(p)}
        q = selection_set.pop()
        return q

    def _K(self, q, ker, aequiv):
        """
        Compute K_q = {p in ker | p ~ q}.

        :param int q: state q for K_q
        :param set ker: set of all kernel states
        :param dictionary aequiv: partition of almost equivalence
        :returns: K_q
        :rtype: set
        """
        for p, equiv_class in aequiv.iteritems():
            if q in equiv_class:
                return ker & equiv_class

    def _P(self, pre, aequiv):
        """
        Compute P_~ = {B in aequiv | B subset of pre}.

        :param set pre: set of all preamble states
        :param dictionary aequiv: partition of almost equivalence
        :returns: P_~
        :rtype: dictionary
        """
        return {q:equiv_class for q, equiv_class in aequiv.iteritems() if equiv_class <= pre}

    def _delta(self, q, a):
        """
        Use delta with state name instead of index.

        :param object q: starting state
        :param string a: symbol
        :returns: delta(q, a)
        :rtype: object
        """
        return self.States[self.delta[self.stateIndex(q)][a]]

    def dup(self):
        """
        Duplicate the basic structure into a new DFA. Basically a copy.deep.

        :rtype: DFA
        """
        new = DFA()
        new.setSigma(self.Sigma)
        new.States = self.States[:]
        new.Initial = self.Initial
        new.Final = self.Final.copy()
        for s in self.delta.keys():
            new.delta[s] = {}
            for c in self.delta[s]:
                new.delta[s][c] = self.delta[s][c]
        return new

    @staticmethod
    def fromFAdoDFA(dfa):
        """
        Creates DFA from FAdo.DFA object.

        :rtype: DFA
        """
        new = DFA()
        new.setSigma(dfa.Sigma)
        new.States = dfa.States[:]
        new.Initial = dfa.Initial
        new.Final = dfa.Final.copy()
        for s in dfa.delta.keys():
            new.delta[s] = {}
            for c in dfa.delta[s]:
                new.delta[s][c] = dfa.delta[s][c]
        return new

    @staticmethod
    def fromFile(filename):
        """
        Reads a DFA from file in FAdo's file format.

        :rtype: list of DFA
        """
        dfas = readFromFile(filename)
        return [DFA.fromFAdoDFA(dfa) for dfa in dfas]

    def toFile(self, filename, write_mode):
        saveToFile(filename, [self], write_mode)

    def __str__(self):
        string = "States: %s\n" % list(self.States)
        string += "Sigma: %s\n" % list(self.Sigma)
        string += "Initial state: %s\n" % self.States[self.Initial]
        string += "Final states: %s\n" % [self.States[q] for q in self.Final]
        string += "Delta:\n"

        alphabet = list(self.Sigma)
        for a in alphabet:
            string += "\t%s" % a
        string += "\n"
        for q in self.States:
            string += "%s" % q
            for a in alphabet:
                string += "\t%s" % self._delta(q, a)
            string += "\n"
        return string

    def __repr__(self):
        return str(self)
