# Implementierung der distanzminimalen Hyperminimierung in Python #

aus: [1] Maletti, Andreas und Daniel Quernheim: Optimal hyper-minimization. International Journal of Foundations of Computer Science, 22(08):1877-1891, 2011.

Die Implementierung basiert auf dem Python-Package FAdo: http://fado.dcc.fc.up.pt/

### Anforderungen ###

* Python 2
* FAdo 1.3.2

### Anleitung ###

Die auszuführende Datei ist "hyperoptimize.py". Als Eingabe wird eine Datei erwartet, die den zu hyperminimierenden DFA enthält. Das Format der Datei ist von FAdo definiert: http://www.dcc.fc.up.pt/~rvr/FAdoDoc/index.html#fio.readFromFile
In "/examples" sind Beispieldateien gegeben:

* "my_example.fa" enthält den Beispielautomaten aus meiner Bachelorarbeit
* "optimal_example.fa" enthält den Beispielautomaten aus [1]

Parameter:

* -i: Nullbasierter Index des zu hyperminimierenden DFA, falls die Eingabedatei mehrere DFA enthält
* -o: Der distanzminimal hyperminimale DFA wird in die hier angegebene Datei geschrieben.
* -d: Distanz zwischen dem hyperminimalen DFA und dem Originalautomten ausgeben.
* -s: Wörter in der Differenz zwischen dem hyperminimalen DFA und dem Originalautomaten ausgeben.
* -p: Präfixsprache aller Präambelzustände des minimalen DFA ausgeben.
* -q: Wörter in der Differenz aller Paare f-äquivalenter Zustände ausgeben.
* -h: Hilfe anzeigen (diese Parameterliste)

### Beispiel ###

In Windows führt beispielsweise der Programmaufruf

```
python hyperoptimize.py examples/optimal_example.fa -i 0 -o hyperoptimal.fa -d -s -d -p -q
```

dazu, dass der distanzminimal hyperminimale DFA in die Datei "hyperoptimal.fa" geschrieben wird und zu folgender Ausgabe:

```
==========MINIMAL-INPUT-DFA==========
States: ['A', 'C', 'B', 'E', 'D', 'G', 'F', 'I', 'H', 'K', 'J', 'M', 'L', '0']
Sigma: ['a', 'b']
Initial state: 0
Final states: ['A', 'C', 'M', 'H', 'K']
Delta:
        a       b
A       B       C
C       G       H
B       C       D
E       I       F
D       G       H
G       K       H
F       J       E
I       K       J
H       L       J
K       L       L
J       L       J
M       M       M
L       M       M
0       A       E


==========HYPEROPTIMAL-DFA==========
States: ['A', 'C', 'B', 'E', 'F', 'I', 'K', 'J', 'M', 'L', '0']
Sigma: ['a', 'b']
Initial state: 0
Final states: ['A', 'C', 'K', 'M']
Delta:
        a       b
A       B       C
C       I       J
B       C       C
E       I       F
F       J       E
I       K       J
K       L       L
J       L       J
M       M       M
L       M       M
0       A       E


==========DISTANCE==========
7

==========DISTANCE-STRINGS==========
abab
aabab
abb
aaab
aab
aabb
aaaab

==========PREFIXES==========
A: ['a']
C: ['ab', 'aaa']
B: ['aa']
D: ['aab']
G: ['aaaa', 'aba', 'aaba']
H: ['abab', 'aabab', 'abb', 'aabb', 'aaab', 'aaaab']
0: ['']

==========STATE-DIFFERENCES==========
(C, D): ['']
(I, H): ['', 'a', 'aa', 'ab']
(I, J): ['a', 'aa', 'ab']
(I, G): ['b']
(H, J): ['']
(H, G): ['', 'a', 'aa', 'ab', 'b']
(J, G): ['a', 'aa', 'ab', 'b']
(K, M): ['a', 'b']
(K, L): ['', 'a', 'b']
(M, L): ['']
```