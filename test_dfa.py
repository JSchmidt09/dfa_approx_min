import pytest

from dfa import DFA
from FAdo.fio import readFromFile

ex = DFA.fromFile("examples/optimal_example.fa")[0]
ex = ex.minimal()
comp, center, mark = ex.computeKernel()
ker = set([ex.States[s] for s in mark])
pre = set(ex.States)-ker
aequiv = ex.aEquiv()

ex._initializeMerge()

print ex
print ex.error_count
print ex.error_strings

def test_compErrorCount():
    for b in aequiv:
        e_class = aequiv[b].copy()
        while e_class:
            q = e_class.pop()
            for p in e_class:
                ex.compErrorCount(p,q)

    assert ex.error_count["C"]["D"] == 1

    assert ex.error_count["G"]["H"] == 5
    assert ex.error_count["G"]["I"] == 1
    assert ex.error_count["G"]["J"] == 4
    assert ex.error_count["H"]["I"] == 4
    assert ex.error_count["H"]["J"] == 1
    assert ex.error_count["I"]["J"] == 3

    assert ex.error_count["K"]["L"] == 3
    assert ex.error_count["K"]["M"] == 2
    assert ex.error_count["L"]["M"] == 1

def test_compErrorStrings():
    for b in aequiv:
        e_class = aequiv[b].copy()
        while e_class:
            q = e_class.pop()
            for p in e_class:
                ex.compErrorStrings(p,q)

    assert ex.error_strings["C"]["D"] == {""}

    assert ex.error_strings["G"]["H"] == {"", "a", "aa", "ab", "b"}
    assert ex.error_strings["G"]["I"] == {"b"}
    assert ex.error_strings["G"]["J"] == {"a", "aa", "ab", "b"}
    assert ex.error_strings["H"]["I"] == {"", "a", "aa", "ab"}
    assert ex.error_strings["H"]["J"] == {""}
    assert ex.error_strings["I"]["J"] == {"a", "aa", "ab"}

    assert ex.error_strings["K"]["L"] == {"", "a", "b"}
    assert ex.error_strings["K"]["M"] == {"a", "b"}
    assert ex.error_strings["L"]["M"] == {""}


def test_compPrefixCount():
    for q in pre:
        ex.compPrefixCount(q)

    assert ex.prefix_count["0"] == 1
    assert ex.prefix_count["A"] == 1
    assert ex.prefix_count["B"] == 1
    assert ex.prefix_count["C"] == 2
    assert ex.prefix_count["D"] == 1
    assert ex.prefix_count["G"] == 3
    assert ex.prefix_count["H"] == 6

def test_compPrefixStrings():
    for q in pre:
        ex.compPrefixStrings(q)

    assert ex.prefix_strings["0"] == {""}
    assert ex.prefix_strings["A"] == {"a"}
    assert ex.prefix_strings["B"] == {"aa"}
    assert ex.prefix_strings["C"] == {"aaa", "ab"}
    assert ex.prefix_strings["D"] == {"aab"}
    assert ex.prefix_strings["G"] == {"aba", "aaaa", "aaba"}
    assert ex.prefix_strings["H"] == {"abb", "aaab", "aabb", "abab", "aaaab", "aabab"}

def test_compFinality():
    expected_e = ex.global_distance + 1
    expected_eS = ex.global_distance_strings | {"aab"}
    assert ex.compFinality(set(["C", "D"])) == "C"
    assert ex.global_distance == expected_e
    assert ex.global_distance_strings == expected_eS

def test_P():
    P = ex._P(pre, aequiv)

    assert P["0"] == set(["0"])
    assert P["A"] == set(["A"])
    assert P["B"] == set(["B"])
    if "C" in P:
        assert P["C"] == set(["C", "D"])
    elif "D" in P:
        assert P["D"] == set(["D", "D"])
    else:
        pytest.fail()
    assert not set(P.keys()) & set(["G", "H", "I", "J", "K", "L", "M"])

def test_K():
    K = {}
    for q in ex.States:
        K[q] = ex._K(q, ker, aequiv)

    assert K["0"] == set()
    assert K["A"] == set()
    assert K["B"] == set()
    assert K["C"] == set()
    assert K["D"] == set()
    assert K["E"] == set(["E"])
    assert K["F"] == set(["F"])
    assert K["G"] == set(["I", "J"])
    assert K["H"] == set(["I", "J"])
    assert K["I"] == set(["I", "J"])
    assert K["J"] == set(["I", "J"])
    assert K["K"] == set(["K", "L", "M"])
    assert K["L"] == set(["K", "L", "M"])
    assert K["M"] == set(["K", "L", "M"])

def test_hyperoptimal():
    hyperopt_dfa = DFA()

    o_index = hyperopt_dfa.addState('0')
    a_index = hyperopt_dfa.addState('A')
    b_index = hyperopt_dfa.addState('B')
    c_index = hyperopt_dfa.addState('C')
    e_index = hyperopt_dfa.addState('E')
    f_index = hyperopt_dfa.addState('F')
    i_index = hyperopt_dfa.addState('I')
    j_index = hyperopt_dfa.addState('J')
    k_index = hyperopt_dfa.addState('K')
    l_index = hyperopt_dfa.addState('L')
    m_index = hyperopt_dfa.addState('M')

    hyperopt_dfa.setSigma(['a', 'b'])

    hyperopt_dfa.addTransition(o_index, 'a', a_index)
    hyperopt_dfa.addTransition(o_index, 'b', e_index)
    hyperopt_dfa.addTransition(a_index, 'a', b_index)
    hyperopt_dfa.addTransition(a_index, 'b', c_index)
    hyperopt_dfa.addTransition(b_index, 'a', c_index)
    hyperopt_dfa.addTransition(b_index, 'b', c_index)
    hyperopt_dfa.addTransition(c_index, 'a', i_index)
    hyperopt_dfa.addTransition(c_index, 'b', j_index)
    hyperopt_dfa.addTransition(e_index, 'a', i_index)
    hyperopt_dfa.addTransition(e_index, 'b', f_index)
    hyperopt_dfa.addTransition(f_index, 'a', j_index)
    hyperopt_dfa.addTransition(f_index, 'b', e_index)
    hyperopt_dfa.addTransition(i_index, 'a', k_index)
    hyperopt_dfa.addTransition(i_index, 'b', j_index)
    hyperopt_dfa.addTransition(j_index, 'a', l_index)
    hyperopt_dfa.addTransition(j_index, 'b', j_index)
    hyperopt_dfa.addTransition(k_index, 'a', l_index)
    hyperopt_dfa.addTransition(k_index, 'b', l_index)
    hyperopt_dfa.addTransition(l_index, 'a', m_index)
    hyperopt_dfa.addTransition(l_index, 'b', m_index)
    hyperopt_dfa.addTransition(m_index, 'a', m_index)
    hyperopt_dfa.addTransition(m_index, 'b', m_index)

    hyperopt_dfa.setInitial(o_index)

    hyperopt_dfa.addFinal(a_index)
    hyperopt_dfa.addFinal(c_index)
    hyperopt_dfa.addFinal(k_index)
    hyperopt_dfa.addFinal(m_index)

    calc_dfa, error_count, error_strings = ex.hyperoptimal()

    assert error_count == 7
    assert error_strings == {"aaaab", "aaab", "aab", "aabab", "aabb", "abab", "abb"}
    assert calc_dfa == hyperopt_dfa
