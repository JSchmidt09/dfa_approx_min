import argparse
import os
from itertools import combinations

from dfa import DFA

parser = argparse.ArgumentParser(description = "Hyperoptimization of DFAs")

parser.add_argument("dfa", type=str,
    help="The file that contains the DFA which should be hyperoptimized")
parser.add_argument("-i", "--index", default=0, type=int,
    help="Zero based index in the input file of the dfa that should be hyperoptimized")
parser.add_argument("-o", "--output_file",
    help="If specified, the hyperoptimal DFA is written to this file")
parser.add_argument("-d", "--distance", action="store_true",
    help="Output the distance between the input and the hyperoptimal DFA")
parser.add_argument("-s", "--distance_strings", action="store_true",
    help="Output the strings in the difference between the input and the hyperoptimal DFA")
parser.add_argument("-p", "--prefixes", action="store_true",
    help="Output the prefixes for all preamble states of the minimal DFA")
parser.add_argument("-q", "--state_differences", action="store_true",
    help="Output the strings in the difference between all f-equivalent states in the minimal DFA")

args = parser.parse_args()

assert os.path.isfile(args.dfa), "The input file does not exist."
dfa = DFA.fromFile(args.dfa)[args.index]
dfa = DFA.fromFAdoDFA(dfa.minimal())
print "==========MINIMAL-INPUT-DFA=========="
print dfa
print

opt_dfa, distance, distance_strings = dfa.hyperoptimal()
print "==========HYPEROPTIMAL-DFA=========="
print opt_dfa
print

if args.distance:
    print "==========DISTANCE=========="
    print distance
    print

if args.distance_strings:
    print "==========DISTANCE-STRINGS=========="
    for string in distance_strings:
        print string
    print

if args.prefixes:
    print "==========PREFIXES=========="
    comp, center, mark = dfa.computeKernel()
    ker = set([dfa.States[s] for s in mark])
    pre = set(dfa.States)-ker
    dfa._initializeMerge()
    for q in pre:
        prefixes = dfa.compPrefixStrings(q)
        print "%s: %s" % (q, list(prefixes))
    print

if args.state_differences:
    print "==========STATE-DIFFERENCES=========="
    dfa._initializeMerge()
    for q, e_class in dfa.aEquiv().iteritems():
        for q1, q2 in combinations(e_class, 2):
            print "(%s, %s): %s" % (q1, q2, list(dfa.compErrorStrings(q1, q2)))

if args.output_file:
    opt_dfa.toFile(args.output_file, "w")
